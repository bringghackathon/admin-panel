const rewireTypescript = require('react-app-rewire-typescript');
const {injectBabelPlugin} = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  config = rewireTypescript(config, env);

  config = injectBabelPlugin(
    ["import", {"libraryName": "antd", "style": true}],
    config,
  );

  config = rewireLess.withLoaderOptions({
    modifyVars: {
      "@primary-color": "#ad4e4e",
      "@font-size-base": "17px"
    },
    javascriptEnabled: true,
  })(config, env);

  return config;
};