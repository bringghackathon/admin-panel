import * as React from "react";
import { Redirect, Route, RouterProps, Switch, withRouter } from "react-router";
import { BrowserRouter } from "react-router-dom";
import {
  Layout, Menu, Breadcrumb, Icon
} from "antd";
import { Scaffolding } from "./Scaffolding";
import MissionList from "./MissionList";
import AddMissionView from "./AddMissionView";
import RewardList from "./RewardList";

const {
  Header, Content, Footer, Sider
} = Layout;

const SubMenu = Menu.SubMenu;

interface IProps {

}

interface IState {
  collapsed: boolean
}

class ApplicationLayout extends React.Component<RouterProps & IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: false
    };
  }

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  selectedKeys(): string[] | undefined {
    const { pathname } = this.props.history.location;
    if (pathname.startsWith("/rewards")) {
      return ["rewards"];
    }
    if (pathname.startsWith("/missions")) {
      return ["missions"];
    }

    return undefined;
  }

  clickedSidebarItem = (e) => {
    const history = this.props.history;
    history.push(`/${e.key}`);
  };

  render() {
    const selectedKeys = this.selectedKeys();
    if (!selectedKeys) {
      return <Redirect to={"/missions"}/>;
    }

    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo"/>
          <Menu theme="dark" selectedKeys={selectedKeys} mode="inline">

            <Menu.Item key="missions" onClick={this.clickedSidebarItem}>
              <Icon type="rocket"/>
              <span>Missions</span>
            </Menu.Item>

            <Menu.Item key="rewards" onClick={this.clickedSidebarItem}>
              <Icon type="crown" />
              <span>Rewards</span>
            </Menu.Item>

          </Menu>
        </Sider>

        <Layout>
          <Content style={{ margin: "16px 32px" }}>
            <Switch>
              <Route exact path={"/missions"} component={MissionList}/>
              <Route exact path={"/rewards"} component={RewardList}/>
              <Route component={Scaffolding}/>
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default withRouter(ApplicationLayout);