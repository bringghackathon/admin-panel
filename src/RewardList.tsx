import * as React from "react";
import { Button, Col, message, Row, Spin, Typography } from "antd";
import { Reward } from "./Models/Reward";
import RewardsService from "./Services/RewardsService";
import { RewardView } from "./RewardView";

const { Title } = Typography;

interface IProps {

}

interface IState {
  rewards: Reward[] | undefined,
}

export default class RewardList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      rewards: undefined,
    };
  }

  componentWillMount() {
    RewardsService.shared.getRewards().then((rewards) =>{
      this.setState({rewards})
    }).catch((error) => message.error(error.message));
  }

  loadingView = () => {
    return <Spin size="large" style={{ position: "absolute", marginTop: "20%", marginLeft: "38%" }}/>;
  };

  listView = () => {
    const { rewards } = this.state;
    if (!rewards) {
      return <div></div>;
    }

    return rewards.map((reward, index) => {
      return <React.Fragment key={`reward_key_${index}`}>
        <RewardView reward={reward}/>
        <div style={{ height: 20 }}/>
      </React.Fragment>;
    });
  };

  render() {
    const { rewards } = this.state;

    return <React.Fragment>
      <Title level={2}>Rewards:</Title>

      {rewards === undefined ? this.loadingView() : this.listView()}
    </React.Fragment>;
  }
}