import * as React from "react";
import { Form, Input, Modal, Radio, Select, Typography } from "antd";
import { FormComponentProps } from "antd/es/form";
import { MissionType } from "./Models/MissionType";

const { Title } = Typography;
const { Option } = Select;

interface IProps {
  visible: boolean;
  onCancel: any;
  onCreate: any;
}

interface IState {

}

class AddMissionView extends React.Component<FormComponentProps & IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {};
  }

  achievementPartsTitle = (type: MissionType): string | undefined => {
    switch (type) {
      case MissionType.customer_rating_greater_than:
        return "Number of times to get a good rating";
      case MissionType.finish_order_in_time:
        return "Number of times to finish order in time";
      case MissionType.join_rewards_club:
        return undefined;
      case MissionType.time_on_site_less_than:
        return "Number of times to be on site";
    }
  };

  missionParameterTitle = (type: MissionType): string | undefined => {
    switch (type) {
      case MissionType.customer_rating_greater_than:
        return "Minimum rating from the customer";
      case MissionType.finish_order_in_time:
        return undefined;
      case MissionType.join_rewards_club:
        return undefined;
      case MissionType.time_on_site_less_than:
        return "Maximum time on site";
    }
  };

  missionTypeOptionTitle = (type: MissionType): string => {
    switch (type) {
      case MissionType.customer_rating_greater_than:
        return "Customer rating";
      case MissionType.finish_order_in_time:
        return "Finish order in time";
      case MissionType.join_rewards_club:
        return "Join reward club";
      case MissionType.time_on_site_less_than:
        return "Time on site";
    }
  };

  render() {
    const {
      getFieldDecorator,
      getFieldValue
    } = this.props.form;

    const { visible, onCancel, onCreate } = this.props;
    const missionTypeValue = getFieldValue("mission_type");
    const achievementPartsTitle = missionTypeValue && this.achievementPartsTitle(missionTypeValue);
    const missionParameterTitle = missionTypeValue && this.missionParameterTitle(missionTypeValue);

    return <Modal
      visible={visible}
      title="Create a new collection"
      okText="Create"
      onCancel={onCancel}
      onOk={onCreate}
    >
      <Form layout="vertical">

        <Form.Item>
          {getFieldDecorator("mission_type")(
            <Select placeholder="Please select a mission type">
              {Object.keys(MissionType).map((missionType: any) => {
                return <Option key={missionType}
                               value={missionType}>{this.missionTypeOptionTitle(missionType)}</Option>;
              })}
            </Select>
          )}
        </Form.Item>

        {missionTypeValue &&
        <Form.Item label="Mission Title">
          {getFieldDecorator("title", {
            rules: [{ required: true, message: "Please input the title of the mission!" }]
          })(
            <Input/>
          )}
        </Form.Item>
        }

        {achievementPartsTitle &&
        <Form.Item label={achievementPartsTitle}>
          {getFieldDecorator("achievement_parts", {
            rules: [{ required: !!achievementPartsTitle, message: "Cannot be empty!" }]
          })(
            <Input/>
          )}
        </Form.Item>
        }

        {missionParameterTitle &&
        <Form.Item label={missionParameterTitle}>
          {getFieldDecorator("mission_parameter", {
            rules: [{ required: !!missionParameterTitle, message: "Cannot be empty!" }]
          })(
            <Input/>
          )}
        </Form.Item>
        }

        {missionTypeValue &&
        <Form.Item label="Points Reward for mission">
          {getFieldDecorator("point_reward", {
            rules: [{ required: true, message: "Please input the number of points for the mission!" }]
          })(
            <Input/>
          )}
        </Form.Item>
        }
      </Form>
    </Modal>;
  }
}

export default Form.create({ name: "form_in_modal" })(AddMissionView);