import * as React from "react";
import { Avatar, Card, message, Switch } from "antd";
import { Reward } from "./Models/Reward";

const { Meta } = Card;

interface IProps {
  reward: Reward
}

interface IState {

}

export class RewardView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    const {reward} = this.props;

    return <Card>
      <Meta
        title={reward.title}
        description={
          <React.Fragment>
            <p style={{marginBottom: 4}}> {reward.subtitle}</p>
            <p>Number of points needed: {reward.number_of_points}</p>
          </React.Fragment>
            }
        avatar={<img style={{maxHeight: 100, maxWidth: 200}} alt="Reward" src={reward.image_url} />}
      />
    </Card>
  }
}