import { MissionType } from "./MissionType";

export type Mission = {
  id: string,
  image_url: string,
  title: string,
  achievement_parts: number,
  point_reward: number,
  is_enabled: boolean,
  type: MissionType,
  mission_parameter: number | null
}