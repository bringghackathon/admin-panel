export type Reward = {
  title: string,
  subtitle: string,
  image_url: string,
  number_of_points: number
}