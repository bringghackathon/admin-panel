import { HTTPService } from "./HTTPService";
import { Mission } from "../Models/Mission";
import { BehaviorSubject } from "rxjs";

export default class MissionsService extends HTTPService {
  private static sharedInstance: MissionsService;
  public static get shared(): MissionsService {
    return this.sharedInstance || (this.sharedInstance = new this());
  }

  constructor() {
    super();

    this.missions = new BehaviorSubject(undefined);
    this.didStartLoadingMessages = false;
  }

  public missions: BehaviorSubject<Mission[] | undefined>;
  private didStartLoadingMessages: boolean;

  public async loadMissionsIfNeeded() {
    if (this.didStartLoadingMessages) {
      return;
    }

    this.didStartLoadingMessages = true;
    await this.loadMissions();
  }

  public async updateMissionEnabled(missionId: string, isEnabled: boolean): Promise<void> {
    await this.post('missions/enabled', {
      mission_id: missionId,
      is_enabled: isEnabled
    });

    await this.loadMissions()
  }

  public async createMission(payload): Promise<void> {
    await this.post('missions', payload);
    await this.loadMissions()
  }

  private async loadMissions(): Promise<void> {
    const response: any = await this.getURL("missions");
    this.missions.next(response.missions);
  }
}