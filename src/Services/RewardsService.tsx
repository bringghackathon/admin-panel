import { HTTPService } from "./HTTPService";
import { Reward } from "../Models/Reward";

export default class RewardsService extends HTTPService {
  private static sharedInstance: RewardsService;
  public static get shared(): RewardsService {
    return this.sharedInstance || (this.sharedInstance = new this());
  }

  constructor() {
    super();
  }

  public async getRewards(): Promise<Reward[]> {
    const response = await this.getURL("rewards");
    return response.rewards;
  }
}