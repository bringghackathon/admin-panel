export class User {
  readonly username: string;
  readonly deviceId: string;

  constructor(json: any) {
    this.username = json.username;
    this.deviceId = json.deviceId;
  }
}