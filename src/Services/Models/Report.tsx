export type ManualReport = {
  reportedAt: string;
  startOrStop: boolean;
  activityType: string;
  deviceId: string;
}

export type CoreMotionReport = {
  walking: boolean,
  stationary: boolean,
  startDate: string
  automotive: boolean
  deviceId: string;
  confidence: number;
}

export type Reports = {
  manualReports: ManualReport[]
  coreMotionReports: CoreMotionReport[]
}