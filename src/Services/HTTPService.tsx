import {User} from "./Models/User";
import {Reports} from "./Models/Report";
import { Mission } from "../Models/Mission";

export class HTTPService {
  private get baseURL(): string {
    return "https://hackathon-bringg-backend.herokuapp.com/api/"
  }

  protected constructor() {
  }

  private createHeaders(): any {
    return {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      "Authorization": "9999999"
    };
  }

  protected async getURL(path: string): Promise<any> {
    const response = await fetch(this.baseURL + path, {
      headers: this.createHeaders()
    });
    const responseJSON = await response.json();

    if (!response.ok) {
      console.log(JSON.stringify(responseJSON));
      throw new Error(responseJSON.error.message || `GET request failed with code ${response.status}`);
    }

    return responseJSON;
  }

  protected async post(path: string, body: any): Promise<any> {
    const response = await fetch(this.baseURL + path, {
      body: JSON.stringify(body),
      headers: this.createHeaders(),
      method: "POST",
    });

    const responseJSON = await response.json();

    if (!response.ok) {
      const error = Error((responseJSON.error && responseJSON.error.message )|| `Request failed with code: ${response.status}`);
      error['httpCode'] = response.status;
      error['code'] = responseJSON && responseJSON.error.errorCode;
      console.error(error);
      console.log(`response: ${JSON.stringify(responseJSON)}`);
      throw error;
    }

    return responseJSON;
  }
}