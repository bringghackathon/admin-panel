import * as React from "react";
import { Button, Col, message, Row, Spin, Typography } from "antd";
import { Mission } from "./Models/Mission";
import { MissionView } from "./MissionView";
import MissionsService from "./Services/MissionsService";
import { Subscription } from "rxjs";
import { RouterProps, withRouter } from "react-router";
import AddMissionView from "./AddMissionView";

const { Title } = Typography;

interface IProps {

}

interface IState {
  missions: Mission[] | undefined,
  isShowingCreate: boolean
}

class MissionList extends React.Component<RouterProps & IProps, IState> {
  missionsSubscription: Subscription;
  formRef: any;

  constructor(props) {
    super(props);

    this.state = {
      missions: undefined,
      isShowingCreate: false
    };
  }

  componentWillMount() {
    MissionsService.shared.loadMissionsIfNeeded()
      .catch((error) => {
        message.error(error.message);
      });

    this.missionsSubscription = MissionsService.shared.missions.subscribe((missions) => {
      this.setState({ missions });
    });
  }

  loadingView = () => {
    return <Spin size="large" style={{ position: "absolute", marginTop: "20%", marginLeft: "38%" }}/>;
  };

  listView = () => {
    const { missions } = this.state;
    if (!missions) {
      return <div></div>;
    }

    return missions.sort((a, b) => { return a.id.localeCompare(b.id) }).map((mission) => {
      return <React.Fragment key={mission.id}>
        <MissionView mission={mission}/>
        <div style={{ height: 20 }}/>
      </React.Fragment>;
    });
  };

  addMissionClicked = () => {
    this.setState({
      isShowingCreate: true
    });
  };

  addMissionViewOnCancel = (e) => {
    this.setState({
      isShowingCreate: false
    });
  };

  addMissionViewOnCreate = (e) => {
    const form = this.formRef.props.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      const payload = {
        ...values,
        achievement_parts: !!values["achievement_parts"] ? parseInt(values["achievement_parts"]) : 1,
        point_reward: parseInt(values["point_reward"]),
        mission_parameter: values["mission_parameter"] && parseInt(values["mission_parameter"])
      };

      form.resetFields();
      this.setState({ isShowingCreate: false });

      MissionsService.shared.createMission(payload).catch((error) => message.error(error.message));
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    const { missions, isShowingCreate } = this.state;

    return <React.Fragment>
      <AddMissionView onCancel={this.addMissionViewOnCancel} onCreate={this.addMissionViewOnCreate}
                      visible={isShowingCreate} wrappedComponentRef={this.saveFormRef}/>

      <Row type="flex" justify="space-around" align="middle">
        <Col span={16}>
          <Title level={2}>Missions:</Title>
        </Col>
        <Col span={8}>
          <Button style={{ float: "right", marginBottom: 8 }} onClick={this.addMissionClicked}>Add Mission</Button>
        </Col>
      </Row>

      {missions === undefined ? this.loadingView() : this.listView()}
    </React.Fragment>;
  }
}

export default withRouter(MissionList);