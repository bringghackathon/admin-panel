import * as React from "react";
import { Mission } from "./Models/Mission";
import { Avatar, Card, message, Switch } from "antd";
import MissionsService from "./Services/MissionsService";

const { Meta } = Card;

interface IProps {
  mission: Mission
}

interface IState {

}

export class MissionView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  isEnabledChanged = (e) => {
    const {mission} = this.props;
    MissionsService.shared.updateMissionEnabled(mission.id, e).catch((error) => message.error(error.message));
  }

  render() {
    const {mission} = this.props;
    const missionDesc = `This mission should be completed ${mission.achievement_parts} times for ${mission.point_reward} points`;

    return <Card
    actions={[<div>Enabled: <Switch checked={mission.is_enabled} onChange={this.isEnabledChanged}/></div>]}
    >
      <Meta
      title={mission.title}
      description={missionDesc}
      />
    </Card>
  }
}