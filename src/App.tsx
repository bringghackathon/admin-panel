import React, { Component } from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import ApplicationLayout from "./ApplicationLayout";

interface IProps {

}

interface IState {

}

class App extends Component<IProps, IState> {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: false
    };
  }

  render() {
    return (
      <BrowserRouter>
        <ApplicationLayout/>
      </BrowserRouter>
    );
  }
}

export default App;